#include <Arduino.h>
#include "../include/Credential.h"
#include "../include/MQTTConnect.h"
#include "../include/Wifi.h"

Credential credential;
Mqtt mqttMain;
Wifi wifiMain;

void setup() {
  Serial.begin(76800);
  wifiMain.setup_wifi();
  mqttMain.setup_mqtt();
  mqttMain.setupI2C();
}

void loop() {
  mqttMain.mqttConnect();
  mqttMain.loopI2C();
  delay(2000);
}