#include <ESP8266WiFi.h> 
#include <Wire.h>
#include <PubSubClient.h>

void setup() {
 Serial.begin(76800);
 Wire.begin();
 Serial.println("ESP start");
}

void loop() {
 Wire.beginTransmission(8); /* begin with device address 8 */
 Wire.write("O");
 Wire.endTransmission();
 delay(2000);
 
 Wire.beginTransmission(8);
 Wire.write("c");
 Wire.endTransmission();

 Wire.requestFrom(8, 16);
 while(Wire.available()){
    char c = Wire.read();
  Serial.print(c);
 }
 Serial.println();
 delay(1000);
}