#include <Arduino.h>
#include <ESP8266WiFi.h>
 
void setup() {
 
  Serial.begin(76800);
 
}
void loop() {

  int numberOfNetworks = WiFi.scanNetworks();
  Serial.println("SCAN");
  delay(1000);
  for(int i =0; i<numberOfNetworks; i++){
 
      Serial.print("Network name: ");
      Serial.println(WiFi.SSID(i));
      Serial.print("Signal strength: ");
      Serial.println(WiFi.RSSI(i));
      Serial.println("-----------------------");
  }
  delay(5000);
}