#include <ESP8266WiFi.h> 

void setup()
{
  Serial.begin(76800);
  Serial.println();

  WiFi.begin("OnePlus 5", "a81b46dda1f");

  Serial.print("Connecting");
  int limit = 30;
  int incremente = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    incremente ++;
    if (incremente>= limit){
      Serial.println("Connection error");
      ESP.deepSleep(10 * 1000000);
    }
  }
  Serial.println();

  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {}