#include "Credential.h"
#include "Sensor.h"

DHT dht(DHT_PIN, DHT_TYPE);

void Sensor::setupSensor() {
  dht.begin();
}

void Sensor::loopsensor() {
  data.humi = dht.readHumidity(false);
  data.temp = dht.readTemperature(false);
  while (isnan(data.humi.toFloat()) || isnan(data.temp.toFloat())) {
    Serial.println("Echec de lecture !");
    delay(500);
    data.humi = String(dht.readHumidity());
    data.temp = String(dht.readTemperature());
  }
  delay(1000);
}