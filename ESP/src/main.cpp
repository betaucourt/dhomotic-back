#include "Credential.h"
#include "Event.h"
#include "Sensor.h"
#include "Photoresistor.h"

Event event;
Sensor sensor;
Photoresistor photoResistor;
void requestEvent();

void setup() {
 pinMode(ledPin, OUTPUT);
 pinMode(lightLed, OUTPUT);
 event.setupEvent();
 sensor.setupSensor();
 Serial.begin(115200);
 Serial.println("Arduino begin");
 Wire.onRequest(requestEvent);
}

void loop() {
  sensor.loopsensor();
  photoResistor.photoLoop();
}

static void requestEvent() {
  String humi = sensor.data.humi;
  String temp = sensor.data.temp;
  String lightSensor = photoResistor.data.photo;
  String toSend = humi + String(";") + temp + String(";") + lightSensor;
  Wire.write(toSend.c_str());
}