#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>

#define DHT_TYPE 11
#define DHT_PIN 7

DHT dht(DHT_PIN, DHT_TYPE);

void setup() {
 dht.begin();
 Serial.begin(115200);
 Serial.println("Arduino begin");
}

void loop() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  while (isnan(h) || isnan(t)) {
    Serial.println("Echec de lecture !");
    delay(500);
    return;
  }
  Serial.print("Humidité : ");
  Serial.println(h);
  delay(1000);
}